package ru.croc.ocp.marathon.current;

import ru.croc.ocp.exam.api.Ocp;

import java.util.OptionalInt;
import java.util.ServiceLoader;
import java.util.ServiceLoader.Provider;

public class OcpQuestionCheck {
    public static void main(String[] args) {
        OptionalInt max = ServiceLoader.load(Ocp.class)
                .stream().map(Provider::get).mapToInt(Ocp::question).max();
        max.ifPresent(question -> System.out.println("Max question is " + question));
        OptionalInt min = ServiceLoader.load(Ocp.class).stream()
                .map(Provider::get).mapToInt(Ocp::question).min();
        min.ifPresent(question -> System.out.println("Min question is " + question));
    }
}
