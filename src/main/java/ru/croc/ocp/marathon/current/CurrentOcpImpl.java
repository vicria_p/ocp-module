package ru.croc.ocp.marathon.current;

import ru.croc.ocp.exam.api.Book;
import ru.croc.ocp.exam.api.Ocp;

public class CurrentOcpImpl implements Ocp {
    public String name() {
        return "Ocp 819";
    }

    public int question() {return 50; }

    public int minute() { return 90; }

    public Book getBook() {
        Book book = new Book();
        book.setName("OCP Java SE 11 Programmer II Study Guide 816 и 817");
        return book;
    }
}
