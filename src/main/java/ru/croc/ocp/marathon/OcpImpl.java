package ru.croc.ocp.marathon;

import ru.croc.ocp.exam.api.Book;
import ru.croc.ocp.exam.api.Ocp;

public class OcpImpl implements Ocp {

    public String name() {
        return "Ocp 815";
    }

    public int question() {
        return 70; }

    public int minute() {
        return 180;
    }

    public Book getBook() {
        Book book = new Book();
        book.setName("OCP Java SE 11 Programmer I Study Guide 815");
        return book;
    }
}
