package ru.croc.ocp.student;

import ru.croc.ocp.exam.api.Ocp;
import ru.croc.ocp.exam.util.OcpFinder;

import java.util.List;
import java.util.stream.Collectors;

public class Student {
    public static void main(String[] args) {
        List<Ocp> ocps = OcpFinder.findAllOcp();
        List<String> ocpNamesList = ocps.stream()
                .map(Ocp::name)
                .collect(Collectors.toList());
        String ocpNames = ocps.isEmpty() ? "" : "(" + String.join(",", ocpNamesList) + ")";

        System.out.println("Now you have " + ocps.size() + " OCP exams. " + ocpNames);
    }

}
