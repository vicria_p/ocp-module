package ru.croc.ocp.exam.util;

import ru.croc.ocp.exam.api.Ocp;

import java.util.ArrayList;
import java.util.List;
import java.util.ServiceLoader;

public class OcpFinder {

    public static List<Ocp> findAllOcp() {
        List<Ocp> ocps = new ArrayList<>();
        ServiceLoader<Ocp> loader = ServiceLoader.load(Ocp.class);
        for (Ocp ocp : loader)
            ocps.add(ocp);
        return ocps;
    } }
