package ru.croc.ocp.exam.api;

public interface Ocp {

    String name();

    int question();

    int minute();

    Book getBook();


}
